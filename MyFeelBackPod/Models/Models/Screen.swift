//
//  Screen.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 24/03/2021.
//

import UIKit

class Screen: NSObject {
    
    private var title:String
    private var properties: [String:String]

     init(title:String, properties:[String:String] ) {
        self.title = title
        self.properties = properties
    }
    
    public func getTitle() -> String {
        return self.title
    }
    
    public func setTitle(title:String) {
        self.title = title
    }
    
    public func getProperties() -> [String:String] {
        return self.properties
    }
    
    public func setProperties(properties:[String:String]) {
        self.properties = properties
    }
    
    

}
