//
//  Scenario.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 08/03/2021.
//

import Foundation

struct Scenario  {
    var conditions:[ConditionGroup]
}

extension Scenario: Decodable {
    
    enum ScenarioCodingKeys: String, CodingKey {
        case conditions
    }
    
    init(from decoder: Decoder) throws {
        let scenarioContainer = try decoder.container(keyedBy: ScenarioCodingKeys.self)
       
        conditions = try scenarioContainer.decode([ConditionGroup].self, forKey: .conditions)
       
    }
    
    func evaluate(context:[String:Any]) -> Bool {
        guard let globalCondion = self.conditions.first else {
            return false
        }
        return globalCondion.evaluate(context: context)
    }
}
