//
//  ConditionFactory.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 10/03/2021.
//

import Foundation

class ConditionFactory {
    
    func createConditionFrom(name:String, data:ConditionDatas?) -> Condition {
        
        switch name {
        case "minPageVisited":
            return MinPageVisited(definition: data!)
        
        case "timeoutmin":
            return Timeoutmin(data: data!)
        
        case "varsdatas":
            
            return VarsDatas(data: data!)
        
        case "platform":
            return Platform(data: data!)
            
        case "trueCondition":
       
            return TrueCondition()
            
        case "falseCondition":
            
            return FalseCondition()
        default:
            return FalseCondition()
        }
    }
}

