//
//  MFBHistory.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 24/03/2021.
//

import UIKit

class MFBHistory: NSObject,Codable {
    
    private enum CodingKeys: String, CodingKey {
           case items
       }
    
    required init(from decoder:Decoder) throws {
          let values = try decoder.container(keyedBy: CodingKeys.self)
          items = try values.decode([VisitedScreen].self, forKey: .items)
      }
    
    
    private var items:[VisitedScreen]
    
     init(items:[VisitedScreen] = []) {
        self.items = items
    }
    
    public func getVisitedScreen() -> [VisitedScreen] {
    return items
    }
    
    public func addItem(visitedScreen:VisitedScreen) {
        self.items.append(visitedScreen)
    }
    
    public func setItems(visitedScreens:[VisitedScreen]) {
        self.items = visitedScreens
    }
    
    public func getLastSessionId() -> Int {
        if items.count > 1 {
           var lastVisitedScreen = self.items.first
            for screenToCompare in self.items {
                let lastDate = Date(timeIntervalSince1970: lastVisitedScreen!.getTime())
                let screenToCompareDate = Date(timeIntervalSince1970: screenToCompare.getTime())
                
                if screenToCompareDate > lastDate {
                    lastVisitedScreen = screenToCompare
                }
            }
            return lastVisitedScreen!.getSessionId()
        } else {
            return 1
        }
    }
}
