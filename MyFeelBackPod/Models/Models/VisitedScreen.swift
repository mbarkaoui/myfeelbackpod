//
//  VisitedScreen.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 24/03/2021.
//

import UIKit

public class VisitedScreen:Codable {
    private var title:String
    private var sessionId:Int
    private var properties:[String:String]?
    private var time:Double
    
    
     init(title: String, properties: [String : String]?, time:Double ,sessionId:Int) {
        self.title = title
        self.sessionId = sessionId
        self.properties = properties
        self.time = time
    }
    
    init(title: String, properties: [String : String]?)  {
        self.title = title
        self.properties = properties
        self.sessionId = 0
        self.time = 0
    }

    
    public func setTime(time:Double) -> Double {
        self.time
    }
    
    public func getTime() -> Double {
        return time
    }
    
    public  func getTitle() -> String{
        return title
    }
    
    public func setTitle(title:String) {
        self.title = title
    }
    
    public func setSessionId(sessionId: Int){
        self.sessionId = sessionId
    }
    
    public func getSessionId() -> Int {
        return self.sessionId
    }
    
    public func getProperties() -> [String:String]? {
        return self.properties ?? [:]
    }
    
    public func setProperties(properties:[String:String]) {
        self.properties = properties
    }
    
    
    public func toDictionnary() -> [String:Any] {
        
        let dictionary = [ "title" : getTitle(), "time" : getTime(), "sessionId" : getSessionId()] as [String : Any]
    
    return dictionary
    }
    
    
    
    
    
    
}
