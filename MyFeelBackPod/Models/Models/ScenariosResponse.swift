//
//  ScenariosResponse.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 08/03/2021.
//

import Foundation

struct ScenariosResponse  {

    var scenarios:[Scenario]

}

extension ScenariosResponse: Decodable {
    
    enum ScenarioResponseCodingKeys: String, CodingKey {
        case scenarios
      
    }
    
    init(from decoder: Decoder) throws {
        let scenarioResponseContainer = try decoder.container(keyedBy: ScenarioResponseCodingKeys.self)
       
        scenarios = try scenarioResponseContainer.decode([Scenario].self, forKey: .scenarios)
       
    }
    
    func evaluate(context: [String: Any]) -> Bool {
        for scenario in scenarios {
            let shouldStop = scenario.evaluate(context: context)
            if shouldStop {
                return true
            }
        }
        return false
    }
}
