//
//  Visit.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 24/03/2021.
//

import UIKit

class Visit:Codable {
    private var time:Double
    
    private enum CodingKeys: String, CodingKey {
           case time
       }
    
    required init(from decoder:Decoder) throws {
        
          let values = try decoder.container(keyedBy: CodingKeys.self)
        time = try values.decode(Double.self, forKey: .time)
        
    }
     init(time: Double) {
        self.time = time
    }
    
    func getTime() -> Double {
        return time
    }
    
    func setTime(time:Double) {
        self.time = time
    }
}
