//
//  MFBConfig.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 25/03/2021.
//

import Foundation

public class MFBConfig {
    private var storageDuration:Int
    
    init(storageDuration:Int) {
        self.storageDuration = storageDuration
    }
    
    public func getStorageDuration() -> Int {
        return self.storageDuration
    }
    
    public func setStorageDuration(storageDuration:Int) {
        self.storageDuration = storageDuration
    }
}
