//
//  ConditionDatas.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 10/03/2021.
//

//
//  ConditionDatas.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 10/03/2021.
//

import Foundation

struct ConditionDatas  {
    var operatore: String?
    var values:[String]?
    var operators: [String]?
    var names : [String]?
    var time : String?
    var type: String?
    var unit : String?
    var mode : String?
    var source : String?
    var value : String?
    var regex : String?
    var path : String?
    var duration: String?
}

extension ConditionDatas: Decodable {
    
    enum UserCodingKeys: String, CodingKey {
        case operatore = "operator"
        case values
        case operators = "operators"
        case names
        case time
        case type
        case unit
        case mode
        case source
        case value
        case regex
        case path
        case duration
    }
    
    init(from decoder: Decoder) throws {
        let conditionContainer = try decoder.container(keyedBy: UserCodingKeys.self)
       
        operatore = try conditionContainer.decodeIfPresent(String.self, forKey: .operatore)
        values = try conditionContainer.decodeIfPresent([String].self, forKey: .values)
        operators = try conditionContainer.decodeIfPresent([String].self, forKey: .operators)
        names = try conditionContainer.decodeIfPresent([String].self, forKey: .names)
        time = try conditionContainer.decodeIfPresent(String.self, forKey: .time)
        unit = try conditionContainer.decodeIfPresent(String.self, forKey: .unit)
        type = try conditionContainer.decodeIfPresent(String.self, forKey: .type)
        mode = try conditionContainer.decodeIfPresent(String.self, forKey: .mode)
        source = try conditionContainer.decodeIfPresent(String.self, forKey: .source)
        value = try conditionContainer.decodeIfPresent(String.self, forKey: .value)
        regex = try conditionContainer.decodeIfPresent(String.self, forKey: .regex)
        path = try conditionContainer.decodeIfPresent(String.self, forKey: .path)
        duration = try conditionContainer.decodeIfPresent(String.self, forKey: .duration)

    }
    
    public func getDuration() -> String {
        return self.duration ?? ""
    }
    
    public func getNames() -> [String] {
        return self.names ?? []
    }
    
    public func getTime() -> String {
        return self.time ?? ""
    }
    
    public func getValues() -> [String] {
        return self.values ?? []
    }
    
    public func getOperators() -> [String] {
        return self.operators ?? []
    }
    
    public func getOperator() -> String {
        return self.operatore ?? ""
    }
    
    public func getMode() -> String {
        return self.mode ?? ""
    }
    
    public func getSource() -> String {
        return self.source ?? ""
    }
    
    public func getValue() -> String {
        return self.value ?? ""
    }
    
    public func getRegex() -> String {
        return self.regex ?? ""
    }
    
    public func getPath() -> String {
        return self.path ?? ""
    }
    
    
}
