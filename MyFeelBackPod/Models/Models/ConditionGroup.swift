//
//  ConditionGroupe.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 08/03/2021.
//

import Foundation

struct ConditionGroup  {

    var name: String
    var children: [ConditionGroup]?
    var datas: ConditionDatas?

}

extension ConditionGroup: Decodable {
    
    enum UserCodingKeys: String, CodingKey {
        case name
        case children
        case datas
    }
    
    
    init(from decoder: Decoder) throws {
        let conditionContainer = try decoder.container(keyedBy: UserCodingKeys.self)
       
        name = try conditionContainer.decode(String.self, forKey: .name)
        children = try conditionContainer.decodeIfPresent([ConditionGroup].self, forKey: .children)
        datas = try conditionContainer.decodeIfPresent(ConditionDatas.self, forKey: .datas)
    }
    
    func evaluate(context:[String:Any]) -> Bool {
  
        if (children == nil) {
            let conditionFactory = ConditionFactory()
            let condition = conditionFactory.createConditionFrom(name: name, data: datas)
            return condition.evaluate()
        }
        
        var opArray = [DispatchWorkItem]()
        let semaphore = DispatchSemaphore(value: 0)
        
        if name == "GroupOr" {
            var orResult = false
        
            for condition in children! {
                let operation = DispatchWorkItem {
                    orResult = condition.evaluate(context: context)
                    if orResult {
                        for op in opArray {
                            op.cancel()
                        }
                        semaphore.signal()
                    }
            }
                
                opArray.append(operation)

            }
            print(opArray.count)
            for op in opArray {
                DispatchQueue.global().asyncAfter(deadline: .now(), execute: op)
            }
            
            semaphore.wait()
            return orResult
        }
        
        if name == "GroupAnd" {
            var orResult = true
            for condition in children! {
                orResult = orResult && condition.evaluate(context: context)
            }
            return orResult
        }
        return false
    }
}
