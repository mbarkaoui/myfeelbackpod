//
//  MinPageVisited.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 26/03/2021.
//

import Foundation

class MinPageVisited: Condition {
    
    var data:ConditionDatas

   
    init(definition: ConditionDatas) {
        self.data =  definition
    }
    
    func evaluate() -> Bool {
        
        guard let historySaved:MFBHistory = getSavedHistory() else {
            return false
        }
        
        let mode = data.getMode()
        let source = data.getSource()
        let operatore = data.getOperator()
        let value = data.getValue()
        let path = data.getPath()
        let regex = data.getRegex()
        let names = data.getNames()
        let operators = data.getOperators()
        let values = data.getValues()
        let duration = data.getDuration()
        
        var result = false
        
        let filtredHistory = self.getfilteredHistory(historyToFilter: historySaved, mode: mode, source: source, path: path, regex: regex, names: names, values: values, operators:operators, duration: duration)
        let historySize = filtredHistory.getVisitedScreen().count
        
        switch operatore {
        case "=","==":
            result = historySize == Int(value)!
        case "<":
            result = historySize < Int(value)!
        case ">":
            result =  historySize > Int(value)!
        case ">=":
            result =  historySize  >= Int(value)!
        case "<=":
            result =  historySize  <= Int(value)! 

        default :
            return false
 
        }
        return result

    }
    
    public func getDatas() -> ConditionDatas {
        self.data
    }
    
    public func setDatas(definition : ConditionDatas) {
        self.data = definition
    }
    
    private func getSavedHistory() -> MFBHistory? {
        let defaults = UserDefaults.standard
        guard let historyData = defaults.object(forKey: "history") as? Data else {
                return nil
            }
            
            // Use PropertyListDecoder to convert Data into Player
            guard let history = try? PropertyListDecoder().decode(MFBHistory.self, from: historyData) else {
                return nil
            }
        
        return history

    }
    
    private func getfilteredHistory(historyToFilter: MFBHistory, mode:String, source:String, path:String,regex:String , names: [String], values:[String], operators:[String], duration:String? ) -> MFBHistory{
        
        if mode == "session" {
            let filteredList = self.filteredLastSession(historyToFilter: historyToFilter)
            historyToFilter.setItems(visitedScreens: filteredList)
        }
        
        
        if source == "page" {
            let filteredList = self.filteredRegexAndPath(historyToFilter: historyToFilter, path: path, regex: regex, names: names, operators: operators, values: values)
            historyToFilter.setItems(visitedScreens: filteredList)
        }
        
        if duration != nil {
            let durationNumber = Int(duration!)
            let filteredDurationList =  MFBHistoryUtils.clearOldHistory(visitedItems: historyToFilter.getVisitedScreen(), dateFrom: Date(), days: durationNumber ?? 0)
            
            historyToFilter.setItems(visitedScreens: filteredDurationList)
        }
        
        
        
        return historyToFilter
    
    }
    
    private func filteredRegexAndPath(historyToFilter:MFBHistory, path:String, regex: String, names:[String]?, operators:[String], values:[String]) -> [VisitedScreen]{
        var filteredList = historyToFilter.getVisitedScreen()
        let varsData = VarsDatas(data: self.data)
        
        for (index,screen) in filteredList.enumerated() {
            var result = false
            if path == "" {
                result = true
            } else {
                result  = varsData.evalString(data: path, contextValue: screen.getTitle(), operatore: regex)
            }
            
            if names != nil && names?.count ?? 0 > 0 {
                result = result && hasCorrectProps(screen, names: names!, operators: operators, values: values)
            }
            
            if !result {
                filteredList.removeAll{ value in
                    return value === screen
                }
            }
        }
        
        return filteredList
    }
    
    private func hasCorrectProps(_ screenToEvaluate: VisitedScreen, names:[String], operators:[String], values:[String]) ->Bool {
        let varsData = VarsDatas(data: self.data)

        var evaluationResult = true
        
        for i in 0..<names.count {
            var  propValue:String? = nil
            
            if screenToEvaluate.getProperties() != nil && screenToEvaluate.getProperties()?[names[i]] != nil {
             propValue = screenToEvaluate.getProperties()?[names[i]]
            }
            
            if propValue == nil {
                evaluationResult = false
            } else {
                evaluationResult = evaluationResult && varsData.evalString(data: propValue!, contextValue: values[i], operatore: operators[i])
            }
            
        }
        
        return evaluationResult
    }
    
    private func filteredLastSession(historyToFilter:MFBHistory) -> [VisitedScreen] {
        var  fileteredList = historyToFilter.getVisitedScreen()
        let lastSessionId =  historyToFilter.getLastSessionId()
        for (index,itemToCompare) in fileteredList.enumerated() {
            if itemToCompare.getSessionId() != lastSessionId {
                fileteredList.remove(at: index)
            }
        }
        
        return fileteredList
    }
    
}
