//
//  Platform.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 10/03/2021.
//

import Foundation

class Platform: Condition {
    var data:ConditionDatas
    init(data: ConditionDatas) {
        self.data =  data
    }
    
    func evaluate() -> Bool {
        
        switch data.operatore {
        case "in":
            
            return data.values!.contains("iOS")
        case "notin":
            
         return !data.values!.contains("iOS")
        default:
            return false
        }
    }
}

