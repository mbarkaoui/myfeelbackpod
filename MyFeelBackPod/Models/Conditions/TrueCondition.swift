//
//  TrueCondition.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 10/03/2021.
//

import Foundation

protocol Condition {
    func evaluate() -> Bool
}

class TrueCondition: Condition {
    
   
    func evaluate() -> Bool {
        return true
    }
}
