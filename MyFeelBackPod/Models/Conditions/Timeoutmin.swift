//
//  Timeoutmin.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 17/03/2021.
//

import Foundation
import Defaults

class Timeoutmin: Condition {
    var data:ConditionDatas
    init(data: ConditionDatas) {
        self.data =  data
    }
    
    func evaluate() -> Bool {
      
        guard  let conditionDatasTime = self.data.time else {
            return false
        }
        
       guard  let conditionDatasType = self.data.type else {
            return false
        }
       guard let conditionDatasUnit = self.data.unit else {
            return false
        }
        
        if conditionDatasType == "page" {
            return evaluatePage(timeUnit: conditionDatasUnit, timeToWait: conditionDatasTime)
        }
        
        if conditionDatasType == "site" {
            return evaluateSite(timeUnit: conditionDatasUnit, timeToWait: conditionDatasTime)
        }

        return false
    }
    
    func evaluateSite(timeUnit :String ,timeToWait:String) -> Bool {
       print("date du lancement du module \(Defaults[.initDate])")

        let diffComponents = Calendar.current.dateComponents([.hour, .minute, .second], from: Defaults[.initDate], to: Date())
        guard  let hours = diffComponents.hour else {
            return false
        }
        
        guard let minutes = diffComponents.minute else {
            return false
        }
        
        guard let secondes = diffComponents.second else {
            return false
        }
        
        
        print(secondes)
    
        
        switch timeUnit {
        case "hours":
            if (Int(timeToWait)! - hours) > 0 {
                Thread.sleep(forTimeInterval: TimeInterval((Int(timeToWait)! - hours) * 3600))
                return true
            } else {
                return true
            }
        case "minutes":
            if (Int(timeToWait)! - minutes) > 0 {
                Thread.sleep(forTimeInterval: TimeInterval((Int(timeToWait)! - minutes) * 60))
                return true

            } else {
                return true
            }
        default:
            if (Int(timeToWait)! - secondes) > 0 {
                print("timetosleep \(Int(timeToWait)! - secondes)")
                Thread.sleep(forTimeInterval: TimeInterval(Int(timeToWait)! - secondes))
                return true
            } else {
                return true
            }
        }
        
        return false
        
    }
    
    func evaluatePage(timeUnit :String ,timeToWait:String) -> Bool {
        var duration:Double = 0
        
        switch timeUnit {
        case "seconds":
            duration = Double(timeToWait)!
        case "minutes":
            duration = Double(timeToWait)! * 60
        case "hours":
            duration = Double(timeToWait)! * 3600
        default:
            duration = Double(timeToWait)!
        }
        
        Thread.sleep(forTimeInterval: duration)

        return true
    }
}
