//
//  VarsDatas.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 10/03/2021.
//

import Foundation
import ObjectiveC

class VarsDatas: Condition {
    var data:ConditionDatas
    init(data: ConditionDatas) {
        self.data =  data
    }
    
    func checkArray(item : [AnyObject]) -> Bool {
      return item is Array<Int>
    }
    
    func evaluate() -> Bool {
        
        var evaluationResult = true
        for i in  0..<data.names!.count {
            evaluationResult = evaluationResult && evaluateSubCondition(data: data, index: i)
        }
        return evaluationResult
    }
    
    func evaluateSubCondition(data:ConditionDatas, index:Int) -> Bool{
        
        let context = MyFeelBack.context
        
        guard let variableName = data.names?[index] else {
            return false
        }
        
        guard  let operatore = data.operators?[index] else {
            return false
        }
        
        guard let  valueToCompare = data.values?[index] else {
            return false
        }
        
        if  context[variableName] is Array<AnyObject> {
            return evaluateMultipleValues(definition: valueToCompare, operatore: operatore, contextValue: context[variableName] as! [AnyObject])
        } else {
            return evaluateSimpleValue(data: valueToCompare, operatore: operatore, contextValue: context[variableName] as AnyObject)
        }
    }
    func evaluateMultipleValues(definition:String, operatore:String, contextValue:Array<AnyObject>) -> Bool{
 
        if contextValue.self is Array<String> {
            return evalMultipleString(definition: definition, contextValue: contextValue as! Array<String> , operatore: operatore)
        }
        
        if contextValue.self is Array<ObjCBool> {
            return evalMultipleBool(definition: definition, contextValue: contextValue as! Array<ObjCBool>, operatore: operatore)
        }
        
        if contextValue.self is Array<NSNumber> {
            return evalMultipleNumeric(definition: definition, contextValue: contextValue as! Array<NSNumber>, operatore: operatore)
        }
        
        if contextValue.self is Array<Date> {
            return evalMultipleDate(definition: definition, contextValue: contextValue as! Array<Date>, operatore: operatore)
        }
        
        return false
    }
    
    // MARK: MultipleValues eval
    
    func evalMultipleDate(definition:String, contextValue:[Date], operatore:String) -> Bool {
        switch operatore {
        case "=","!=","<",">","<=",">=","*=":
            var result = false
            for i in 0..<contextValue.count {
                if operatore == "*=" {
                    if evalString(data: definition, contextValue: contextValue[i].string!, operatore: operatore) {
                    result = true
                }
                } else {
                if evalDate(definition: definition, contextValue: contextValue[i], operatore: operatore) {
                    result = true
                }
            }
            }
            return result
        case "!*=":
            var resultArray = [Bool]()
            for i in 0..<contextValue.count {
                
                if evalString(data: definition, contextValue: contextValue[i].string!, operatore: operatore) {
                    resultArray.append(true)
                }
            }
            return resultArray.count == contextValue.count
        
        default:
            return false
    
        }
    }
    
    func evalMultipleNumeric(definition:String, contextValue:[NSNumber], operatore:String) -> Bool {
        switch operatore {
     
       case "=","!=","<",">","<=",">=","*=":
        var result = false
        for i in 0..<contextValue.count {
            if evalNumeric(definition: definition, contextValue: contextValue[i], operatore: operatore) {
                result = true
            }
        }
        return result
        case "!*=":
            var resultArray = [Bool]()
            for i in 0..<contextValue.count {
                
                if evalNumeric(definition: definition, contextValue: contextValue[i], operatore: operatore) {
                    resultArray.append(true)
                }
            }
            return resultArray.count == contextValue.count
        case "in","notin":
            let v = definition.split(separator: ",")
            let arrayNum = v.map{Float($0)}
            let output = contextValue.filter{arrayNum.contains($0.floatValue)}

            if operatore == "in" {
                return output.count > 0
            } else {
                return output.count  == 0
            }

        default:
            return false
        }
    }

    
    func evalMultipleBool(definition:String, contextValue:[ObjCBool], operatore:String) -> Bool {
        switch operatore {
     
       case "=","!=","<",">","<=",">=","*=":
        var result = false
        for i in 0..<contextValue.count {
            if evalBool(definition: definition, contextValue: contextValue[i], operatore: operatore) {
                result = true
            }
        }
        return result
        case "!*=":
            var resultArray = [Bool]()
            for i in 0..<contextValue.count {
                
                if evalBool(definition: definition, contextValue: contextValue[i], operatore: operatore) {
                    resultArray.append(true)
                }
            }
            return resultArray.count == contextValue.count
        case "in":
            let v = definition.split(separator: ",")
            let arrayBool = v.map{$0.bool}
            let output = contextValue.filter{arrayBool.contains($0.boolValue)}
            return output.count > 0
        
        case "notin":
            var notInResult = false
            let v = definition.split(separator: ",")
            let arrayBool = v.map{$0.bool}
            
            for boolContextval in contextValue {
                for splitVal in arrayBool {
                    if splitVal != boolContextval.boolValue {
                        notInResult = true
                    }
                }
            }
            
            return notInResult

        default:
            return false
        }
    }
    
    func evalMultipleString(definition:String, contextValue:[String], operatore:String) -> Bool {
        switch operatore {
        case "=":
            return contextValue.contains(definition)
        case "!=":
            return !(contextValue.contains(definition))
       case "<",">","<=",">=","*=":
        var result = false
        for i in 0..<contextValue.count {
            if evalString(data: definition, contextValue: contextValue[i], operatore: operatore) {
                result = true
            }
        }
        return result
        case "!*=":
            var resultArray = [Bool]()
            for i in 0..<contextValue.count {
                
                if evalString(data: definition, contextValue: contextValue[i], operatore: operatore) {
                    resultArray.append(true)
                }
            }
            return resultArray.count == contextValue.count
        case "in","notin":
            let v = definition.split(separator: ",")
            let arrayString = v.map{String($0)}
            let output = contextValue.filter{arrayString.contains($0)}
            
            if operatore == "in" {
                return output.count > 0
            } else {
                return output.count  == 0
            }

        default:
            return false
        }
    }

    func evaluateSimpleValue(data:String, operatore:String, contextValue:AnyObject ) -> Bool {

        switch contextValue.self {

        case is String:
            print("is string")
            return evalString(data: data, contextValue: contextValue as! String, operatore: operatore)

        case is ObjCBool:
            print("is bool")
            return evalBool(definition: data, contextValue: contextValue as! ObjCBool, operatore: operatore)

        case is Date:
            return evalDate(definition: data, contextValue: contextValue as! Date, operatore: operatore)

        case is NSNumber:
            print("is number")
            return evalNumeric(definition: data, contextValue: contextValue as! NSNumber, operatore: operatore)

        default:
            return false
        }
    }

    // MARK: SimpleValue eval

    func evalDate(definition:String, contextValue:Date , operatore:String) -> Bool {

        switch operatore {
        case "=":
            return definition.date  == contextValue
        case "!=":
            return !(definition.date == contextValue)
        case "<":
            return contextValue < (definition.date)!
        case ">":
            return contextValue > (definition.date)!
        case ">=":
            return contextValue  >= (definition.date)!
        case "<=":
            return contextValue  <= (definition.date)!
        case "*=":
            if let _: Range<String.Index> = contextValue.string?.range(of: definition) {
                //let index: Int = contextValue.distance(from: contextValue.startIndex, to: range.lowerBound)
                return true
            }
            else {
                return false
            }

        case "!*=":
            if let _: Range<String.Index> = contextValue.string?.range(of: definition) {
                //let index: Int = contextValue.distance(from: contextValue.startIndex, to: range.lowerBound)
                return false
            }
            else {
                return true
            }
        case "in":
            var result = false
            let v = definition.split(separator: ",")
            for i in 0..<v.count {
                if v[i].date == contextValue {
                    result = true
                }
            }

            return result
        case "notin":
            var result = true
            let v = definition.split(separator: ",")
            for i in 0..<v.count {
                if v[i].date == contextValue {
                    result = false
                }
            }

            return result
            
        default:
            return false
        }
    }
    
    func evalNumeric(definition:String, contextValue:NSNumber , operatore:String) -> Bool {
        
        switch operatore {
        case "=":
            return Float(definition)! == contextValue.floatValue
        case "!=":
            return !(Float(definition)! == contextValue.floatValue)
        case "<":
            return contextValue.floatValue < Float(definition)!
        case ">":
            return contextValue.floatValue > Float(definition)!
        case ">=":
            return contextValue.floatValue >= Float(definition)!
        case "<=":
            return contextValue.floatValue <= Float(definition)!
        case "*=":
           return Float(definition)! == contextValue.floatValue
        case "!*=":
            return !(Float(definition)! == contextValue.floatValue)
        case "in":
            let v = definition.split(separator: ",")
            for i in 0..<v.count {
                if Float(v[i]) == contextValue.floatValue {
                    return true
                }
            }
            
        case "notin":
            var result = true
            let v = definition.split(separator: ",")
            for i in 0..<v.count {
                if Float(v[i]) == contextValue.floatValue {
                    result = false
                }
            }
            
            return result
            
        default:
            return false
        }
        
        return false
    }
    
    func evalBool (definition:String, contextValue:ObjCBool , operatore:String) -> Bool {
        switch operatore {
        case "=":
            return definition.bool == contextValue.boolValue
        case "!=":
            return !(definition.bool == contextValue.boolValue)
        case "<":
            return Int(truncating: NSNumber(value:contextValue.boolValue)) <  Int(truncating: NSNumber(value: definition.bool!))
        case ">":
            return Int(truncating: NSNumber(value:contextValue.boolValue)) >  Int(truncating: NSNumber(value: definition.bool!))
        case ">=":
            return Int(truncating: NSNumber(value:contextValue.boolValue)) >=  Int(truncating: NSNumber(value: definition.bool!))
        case "<=":
            return Int(truncating: NSNumber(value:contextValue.boolValue)) <= Int(truncating: NSNumber(value: definition.bool!))
        case "*=":
            return definition.bool == contextValue.boolValue
        case "!*=":
            return !(definition.bool == contextValue.boolValue)
        case "in":
            var result = false
            let v = definition.split(separator: ",")
            for i in 0..<v.count {
                if v[i].bool == contextValue.boolValue {
                    result = true
                }
            }
            
            return result
        case "notin":
            var result = true
            let v = definition.split(separator: ",")
            for i in 0..<v.count {
                if v[i].bool == contextValue.boolValue {
                    result = false
                }
            }
            
            return result
        default:
            return true
        }
    }
    
    func evalString(data:String, contextValue:String, operatore:String) -> Bool {
        
        switch operatore {
        case "=","==":
            return data == contextValue
        case "!=":
            return !(data == contextValue)
        case "<":
            return contextValue < data
        case ">":
            return contextValue > data
        case ">=":
            return contextValue >= data
        case "<=":
            return contextValue <= data
        case "*=":
            if let _: Range<String.Index> = contextValue.lowercased().range(of: data.lowercased()) {
                //let index: Int = contextValue.distance(from: contextValue.startIndex, to: range.lowerBound)
                return true
            }
            else {
                return false
            }
            
        case "!*=":
            if let _: Range<String.Index> = contextValue.range(of: data) {
                //let index: Int = contextValue.distance(from: contextValue.startIndex, to: range.lowerBound)
                return false
            }
            else {
                return true
            }
            
            
        case "in":
            let v = data.split(separator: ",")
            for i in 0..<v.count {
                if v[i] == contextValue {
                    return true
                }
            }
            
        case "notin":
            var result = true
            let v = data.split(separator: ",")
            for i in 0..<v.count {
                if v[i] == contextValue {
                    result = false
                }
            }
            
            return result
            
        default:
            return false
        }
        return false
    }
}

