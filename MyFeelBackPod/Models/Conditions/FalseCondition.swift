//
//  FalseCondition.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 10/03/2021.
//

import Foundation

class FalseCondition: Condition {
    
    func evaluate() -> Bool {
        return false
    }
}
