//
//  StringExtension.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 12/03/2021.
//

import Foundation

extension String {
    var bool: Bool? {
        switch self.lowercased() {
        case "true", "1":
            return true
        case "false", "0":
            return false
        default:
            return false
        }
    }
    var date: Date? {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return  dateFormatter.date(from: self)

        
    }
}

extension Substring {
    var bool: Bool? {
        switch self.lowercased() {
        case "true", "1":
            return true
        case "false", "0":
            return false
        default:
            return false
        }
    }
    
    var date: Date? {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"

        return dateFormatter.date(from: String(self))
        
    }
}
