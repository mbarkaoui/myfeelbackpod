//
//  DateExtension.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 14/03/2021.
//

import Foundation
extension Date {
    var string: String? {
     
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        return  dateFormatter.string(from: self)

    }
}
