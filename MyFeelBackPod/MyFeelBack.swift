//
//  MyFeelBack.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 08/03/2021.
//

import Foundation
import Defaults


extension Defaults.Keys {
    static let initDate = Key<Date>("initDate", default:Date())
    static let history = Key<MFBHistory>("MyFeelBack_History", default:MFBHistory())

}

public protocol MyFeelBackProtocol {
    func evalCompletion(resultEval:Bool)
}

public final class MyFeelBack {
    
    let name = "MyFeelBack"
    static var delegate:MyFeelBackProtocol?
    static var scenarioResponse:ScenariosResponse?
    static var context : [String:Any] = [:]
    static var queue = OperationQueue()
    static var evalWorkItem:DispatchWorkItem?
    var currentSessionId:Int?
    var currentScreen:Screen?
    
    private var config:MFBConfig?
    
    
    public init(data:Data) {
        do {
            MyFeelBack.scenarioResponse =  try MyFeelBack.parse(jsonData: data)
            Defaults[.initDate] = Date()
            self.config = MFBConfig(storageDuration: 30)
            
            guard let historySaved = self.getSavedHistory() else {
                return
            }
            
            self.currentSessionId = historySaved.getLastSessionId()
            
        } catch {
            return
        }
    }
    
    public init(urlString:String) {
        
        let session = URLSession.shared
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        
        let task = session.dataTask(with: request){ data, response, error in
            
            guard let data = data else {
                return
            }
            do {
                MyFeelBack.scenarioResponse =  try MyFeelBack.parse(jsonData: data)
                print("")
            } catch {
                return
            }
        }
        
        task.resume()
        
        Defaults[.initDate] = Date()
        self.config = MFBConfig(storageDuration: 30)
        
        guard let historySaved = self.getSavedHistory() else {
            return
        }
        
        self.currentSessionId = historySaved.getLastSessionId()
        
    }
    
    
    public func getConfig() -> MFBConfig{
          return self.config!
      }

    public func setConfig(config: MFBConfig) {
          self.config = config
      }
    
    public func setDelegate(delegate:MyFeelBackProtocol){
        MyFeelBack.delegate = delegate
    }
    
    public func setContext(context: [String:Any]) {
        MyFeelBack.context = context
    }
    
    public func getContext() -> [String:Any] {
        return MyFeelBack.context
    }
    
    public func evaluate(completion:(()->())? = { })  {
        guard  let scenarioResponse = MyFeelBack.scenarioResponse  else {
            return
        }
         resultEvaluation(scenarioResponse,completion: completion)
    }
    
    func resultEvaluation(_ scenarioResponse: ScenariosResponse,completion:(()->())? = { }){
        MyFeelBack.evalWorkItem?.cancel()
        
       let workItem = DispatchWorkItem{
            let result =  scenarioResponse.evaluate(context: self.getContext())
              
              MyFeelBack.delegate?.evalCompletion(resultEval: result)
        }
        
        MyFeelBack.evalWorkItem  = workItem
        
        DispatchQueue.global().asyncAfter(deadline: .now(), execute: MyFeelBack.evalWorkItem!)
        
    }
    
    
    func clearSavedHistory() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "history")
        defaults.synchronize()
    }
    
    func setCurrentScreen(currentScreen:VisitedScreen?,timeNow:Date? = Date() ) {
        
        guard let currentScreen = currentScreen else {
            return
        }
   
        let timeVisit = timeNow?.timeIntervalSince1970
        
        var savedHistory = self.getSavedHistory()
        // update history
        
        if savedHistory == nil {
            savedHistory = MFBHistory()
        }
        
        let visitedScreen = VisitedScreen(title: currentScreen.getTitle(), properties: currentScreen.getProperties() ?? [:], time: timeVisit!, sessionId: self.currentSessionId ?? 1)
        
        
        savedHistory!.addItem(visitedScreen: visitedScreen)

        if savedHistory == nil {
            savedHistory = MFBHistory()
        }
        
        let clearedScreens = MFBHistoryUtils.clearOldHistory(visitedItems: savedHistory!.getVisitedScreen(), dateFrom: Date(), days: self.getConfig().getStorageDuration())
        
        savedHistory?.setItems(visitedScreens: clearedScreens)

        
        // get saved History
        let  defaults = UserDefaults.standard
        
        defaults.setValue(try? PropertyListEncoder().encode(savedHistory), forKey: "history")
        defaults.synchronize()
    }
    
    func setNewSession() {
        
        if let historySaved : MFBHistory = self.getSavedHistory() {
            self.currentSessionId = historySaved.getLastSessionId() + 1
        } else {
            self.currentSessionId = 1
        }
    
    }
    
    
    func getSavedHistory() -> MFBHistory?  {
        let defaults = UserDefaults.standard
        guard let historyData = defaults.object(forKey: "history") as? Data else {
                return nil
            }
            
            // Use PropertyListDecoder to convert Data into Player
            guard let history = try? PropertyListDecoder().decode(MFBHistory.self, from: historyData) else {
                return nil
            }

        return history

        }

    static func parse(jsonData: Data) throws -> ScenariosResponse {
        return try JSONDecoder().decode(ScenariosResponse.self, from: jsonData)
    }
    
}
