//
//  DataUtils.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 02/04/2021.
//

import Foundation

open class DataUtils {
 
    init(){
        
    }
    
     class func getData(urlString:String) -> Data?{
        let session = URLSession.shared
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        var dataResult:Data? = nil
        let task = session.dataTask(with: request){ data, response, error in
            
            guard let data = data else {
               return
            }
            
            dataResult = data
    }
        
        task.resume()
        return dataResult
    
}
}
