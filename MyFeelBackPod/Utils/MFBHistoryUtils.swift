//
//  MFBHistoryUtils.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 25/03/2021.
//

import Foundation

public final class MFBHistoryUtils {
  
    public static func clearOldHistory(visitedItems:[VisitedScreen], dateFrom:Date, days:Int) -> [VisitedScreen] {
        if days == 0 {
            return visitedItems
        }
        
        var filteredItems = visitedItems        
        let currentDate = Date()
        var dateComponent = DateComponents()
        dateComponent.day = -days
        
        guard let previousDate = Calendar.current.date(byAdding: dateComponent, to: currentDate) else {
            return visitedItems
        }

        for (index,itemToCompare) in filteredItems.enumerated() {
            let screenToCompareDate:Date = Date(timeIntervalSince1970: itemToCompare.getTime())
            if screenToCompareDate <= previousDate {
                filteredItems.removeAll{ value in
                    return value === itemToCompare
                }
            }
        }
        
        return filteredItems
 
    }
    
}
