//
//  LocalDataManager.swift
//  MyFeelBackPod
//
//  Created by Malek BARKAOUI on 08/03/2021.
//

import Foundation
class LocalDataManager : NSObject{

    public func readLocalFile(forName name: String) -> Data? {

        do {
            let bundle = Bundle(for: type(of: self))

            if let bundlePath = bundle.path(forResource: name, ofType: "geojson"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        return nil
    }

    public func parse(jsonData: Data) throws -> ScenariosResponse {
        return try JSONDecoder().decode(ScenariosResponse.self, from: jsonData)
    }
}

