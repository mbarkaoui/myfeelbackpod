//
//  MinPageVisitedTests.swift
//  MyFeelBackPodTests
//
//  Created by Malek BARKAOUI on 25/03/2021.
//

import XCTest

import XCTest
import ObjectiveC
import Foundation

@testable import MyFeelBackPod

class MinPageVisitedTests: XCTestCase {
    var myFeelBack: MyFeelBack?
    var dataManager:LocalDataManager?
    var fakeCallBack:FakeCallBack?

    
    override func setUp() {
        fakeCallBack = FakeCallBack()
    }
    
    func testUserDefaults() {
        dataManager = LocalDataManager()
        myFeelBack?.clearSavedHistory()
    }
    
    
    func testMinPageVisited10() {
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioMinPageVisited10") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)
        myFeelBack?.clearSavedHistory()
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack?.setNewSession()

        let currentDate = Date()
        var dateComponent = DateComponents()
        dateComponent.day = -33
        
        guard let visitedScreenDate = Calendar.current.date(byAdding: dateComponent, to: currentDate) else {
            return
        }
        

        let login =  VisitedScreen(title: "login", properties: nil, time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: login, timeNow: visitedScreenDate)
        let menuSportScreen = VisitedScreen(title: "menu", properties: ["univers":"sport"],time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: menuSportScreen,timeNow: visitedScreenDate)
        

        let IntroSportScreen = VisitedScreen(title: "IntroSport", properties: ["univers":"sport"],time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: IntroSportScreen,timeNow: visitedScreenDate)

        let menuPhotoScreen = VisitedScreen(title: "menu", properties: ["univers":"photo"],time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: menuPhotoScreen,timeNow: visitedScreenDate)

        let introPhoto = VisitedScreen(title: "IntroPhoto", properties: ["univers":"photo"],time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: introPhoto,timeNow: visitedScreenDate)

        myFeelBack?.setNewSession()

        dateComponent.day = -32
        guard let visitedScreenDate2 = Calendar.current.date(byAdding: dateComponent, to: currentDate) else {
            return
        }

        
        let login2 = VisitedScreen(title: "login", properties: nil,time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
        myFeelBack?.setCurrentScreen(currentScreen: login2,timeNow: visitedScreenDate2)

        let menuSportScreen2 = VisitedScreen(title: "menu", properties: ["univers":"sport"],time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
        myFeelBack?.setCurrentScreen(currentScreen: menuSportScreen2, timeNow: visitedScreenDate2)

        let IntroSportScreen2 = VisitedScreen(title: "IntroSport", properties: ["univers":"sport"],time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
        myFeelBack?.setCurrentScreen(currentScreen: IntroSportScreen2, timeNow: visitedScreenDate2)

        let menuPhotoScreen2 = VisitedScreen(title: "menu", properties: ["univers":"photo"],time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
        myFeelBack?.setCurrentScreen(currentScreen: menuPhotoScreen2, timeNow: visitedScreenDate2)

        let introPhoto2 = VisitedScreen(title: "IntroPhoto", properties: ["univers":"photo"],time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
     
        myFeelBack?.setCurrentScreen(currentScreen: introPhoto2, timeNow: visitedScreenDate2)
        myFeelBack?.evaluate()

        Thread.sleep(forTimeInterval: Double(1))
        XCTAssertFalse(fakeCallBack!.isWasSuccessfull())

    }
    
    
    func testMinPageVisited9() {
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioMinPageVisited9") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)
        myFeelBack?.clearSavedHistory()
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack?.setNewSession()

        let currentDate = Date()
        var dateComponent = DateComponents()
        dateComponent.day = -10
        
        guard let visitedScreenDate = Calendar.current.date(byAdding: dateComponent, to: currentDate) else {
            return
        }
        
        
        let login =  VisitedScreen(title: "login", properties: nil, time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: login)

        let menuSportScreen = VisitedScreen(title: "menu", properties: ["univers":"sport"],time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: menuSportScreen)
        

        let IntroSportScreen = VisitedScreen(title: "IntroSport", properties: ["univers":"sport"],time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: IntroSportScreen)

        let menuPhotoScreen = VisitedScreen(title: "menu", properties: ["univers":"photo"],time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: menuPhotoScreen)

        let introPhoto = VisitedScreen(title: "IntroPhoto", properties: ["univers":"photo"],time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: introPhoto)

        myFeelBack?.setNewSession()

        dateComponent.day = -9
        guard let visitedScreenDate2 = Calendar.current.date(byAdding: dateComponent, to: currentDate) else {
            return
        }

        
        let login2 = VisitedScreen(title: "login", properties: nil,time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
        myFeelBack?.setCurrentScreen(currentScreen: login2)

        let menuSportScreen2 = VisitedScreen(title: "menu", properties: ["univers":"sport"],time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
        myFeelBack?.setCurrentScreen(currentScreen: menuSportScreen2)

        let IntroSportScreen2 = VisitedScreen(title: "IntroSport", properties: ["univers":"sport"],time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
        myFeelBack?.setCurrentScreen(currentScreen: IntroSportScreen2)

        let menuPhotoScreen2 = VisitedScreen(title: "menu", properties: ["univers":"photo"],time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
        myFeelBack?.setCurrentScreen(currentScreen: menuPhotoScreen2)

        let introPhoto2 = VisitedScreen(title: "IntroPhoto", properties: ["univers":"photo"],time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
     
        myFeelBack?.setCurrentScreen(currentScreen: introPhoto2)
        myFeelBack?.evaluate()

        Thread.sleep(forTimeInterval: Double(1))
        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())

    }
    

    func testMinPageVisited8() {
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioMinPageVisited8") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)
        myFeelBack?.clearSavedHistory()
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack?.setNewSession()

        let currentDate = Date()
        var dateComponent = DateComponents()
        dateComponent.day = -10
        
        guard let visitedScreenDate = Calendar.current.date(byAdding: dateComponent, to: currentDate) else {
            return
        }
        
        
        let login =  VisitedScreen(title: "login", properties: nil, time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: login)

        let menuSportScreen = VisitedScreen(title: "menu", properties: ["univers":"sport"],time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: menuSportScreen)
        

        let IntroSportScreen = VisitedScreen(title: "IntroSport", properties: ["univers":"sport"],time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: IntroSportScreen)

        let menuPhotoScreen = VisitedScreen(title: "menu", properties: ["univers":"photo"],time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: menuPhotoScreen)

        let introPhoto = VisitedScreen(title: "IntroPhoto", properties: ["univers":"photo"],time: visitedScreenDate.timeIntervalSince1970, sessionId: 1)
        myFeelBack?.setCurrentScreen(currentScreen: introPhoto)

        myFeelBack?.setNewSession()

        dateComponent.day = -9
        guard let visitedScreenDate2 = Calendar.current.date(byAdding: dateComponent, to: currentDate) else {
            return
        }

        
        let login2 = VisitedScreen(title: "login", properties: nil,time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
        myFeelBack?.setCurrentScreen(currentScreen: login2)

        let menuSportScreen2 = VisitedScreen(title: "menu", properties: ["univers":"sport"],time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
        myFeelBack?.setCurrentScreen(currentScreen: menuSportScreen2)

        let IntroSportScreen2 = VisitedScreen(title: "IntroSport", properties: ["univers":"sport"],time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
        myFeelBack?.setCurrentScreen(currentScreen: IntroSportScreen2)

        let menuPhotoScreen2 = VisitedScreen(title: "menu", properties: ["univers":"photo"],time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
        myFeelBack?.setCurrentScreen(currentScreen: menuPhotoScreen2)

        let introPhoto2 = VisitedScreen(title: "IntroPhoto", properties: ["univers":"photo"],time: visitedScreenDate2.timeIntervalSince1970, sessionId: 2)
     
        myFeelBack?.setCurrentScreen(currentScreen: introPhoto2)
        myFeelBack?.evaluate()

        Thread.sleep(forTimeInterval: Double(1))
        XCTAssertFalse(fakeCallBack!.isWasSuccessfull())

    }
    
    
    func testMinPageVisited7() {
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioMinPageVisited7") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)
        myFeelBack?.clearSavedHistory()
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack?.setNewSession()
        
        let login = VisitedScreen(title: "login", properties: nil)
        myFeelBack?.setCurrentScreen(currentScreen: login)

        let menuSportScreen = VisitedScreen(title: "menu", properties: ["univers":"sport"])
        myFeelBack?.setCurrentScreen(currentScreen: menuSportScreen)

        let IntroSportScreen = VisitedScreen(title: "IntroSport", properties: ["univers":"sport"])
        myFeelBack?.setCurrentScreen(currentScreen: IntroSportScreen)

        let menuPhotoScreen = VisitedScreen(title: "menu", properties: ["univers":"photo"])
        myFeelBack?.setCurrentScreen(currentScreen: menuPhotoScreen)

        let introPhoto = VisitedScreen(title: "IntroPhoto", properties: ["univers":"photo"])
        myFeelBack?.setCurrentScreen(currentScreen: introPhoto)

        myFeelBack?.evaluate()
        
        Thread.sleep(forTimeInterval: Double(1))

        let history = myFeelBack?.getSavedHistory()

        XCTAssertEqual(history?.getVisitedScreen().count, 5)
        XCTAssertEqual(history?.getLastSessionId(),1)

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
        
    }
    
    func testMinPageVisited6() {
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioMinPageVisited6") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)

        myFeelBack?.clearSavedHistory()
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack?.setNewSession()
        
        let login = VisitedScreen(title: "login", properties: nil)
        myFeelBack?.setCurrentScreen(currentScreen: login)

        let menuSportScreen = VisitedScreen(title: "menu", properties: ["univers":"sport"])
        myFeelBack?.setCurrentScreen(currentScreen: menuSportScreen)

        let IntroSportScreen = VisitedScreen(title: "IntroSport", properties: ["univers":"sport"])
        myFeelBack?.setCurrentScreen(currentScreen: IntroSportScreen)

        let menuPhotoScreen = VisitedScreen(title: "menu", properties: ["univers":"photo"])
        myFeelBack?.setCurrentScreen(currentScreen: menuPhotoScreen)

        let introPhoto = VisitedScreen(title: "IntroPhoto", properties: ["univers":"photo"])
        myFeelBack?.setCurrentScreen(currentScreen: introPhoto)

        myFeelBack?.evaluate()
        
        Thread.sleep(forTimeInterval: Double(1))

        let history = myFeelBack?.getSavedHistory()

        XCTAssertEqual(history?.getVisitedScreen().count, 5)
        XCTAssertEqual(history?.getLastSessionId(),1)

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
        
    }
    
    
    func testMinPageVisited5() {
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioMinPageVisited5") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)
        myFeelBack?.clearSavedHistory()
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack?.setNewSession()
        
        let login = VisitedScreen(title: "login", properties: nil)
        myFeelBack?.setCurrentScreen(currentScreen: login)

        let menuSportScreen = VisitedScreen(title: "menu", properties: ["univers":"sport"])
        myFeelBack?.setCurrentScreen(currentScreen: menuSportScreen)

        let IntroSportScreen = VisitedScreen(title: "IntroSport", properties: ["univers":"sport"])
        myFeelBack?.setCurrentScreen(currentScreen: IntroSportScreen)

        let menuPhotoScreen = VisitedScreen(title: "menu", properties: ["univers":"photo"])
        myFeelBack?.setCurrentScreen(currentScreen: menuPhotoScreen)

        let introPhoto = VisitedScreen(title: "IntroPhoto", properties: ["univers":"photo"])
        myFeelBack?.setCurrentScreen(currentScreen: introPhoto)

        myFeelBack?.evaluate()
        
        Thread.sleep(forTimeInterval: Double(1))

        let history = myFeelBack?.getSavedHistory()

        XCTAssertEqual(history?.getVisitedScreen().count, 5)
        XCTAssertEqual(history?.getLastSessionId(),1)

        XCTAssertFalse(fakeCallBack!.isWasSuccessfull())
        
    }
    
    func testMinPageVisited4() {
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioMinPageVisited4") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)
        
        myFeelBack?.clearSavedHistory()
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack?.setNewSession()
        
        let login = VisitedScreen(title: "login", properties: nil)
        myFeelBack?.setCurrentScreen(currentScreen: login)

        let menuSportScreen = VisitedScreen(title: "menu", properties: ["univers":"sport"])
        myFeelBack?.setCurrentScreen(currentScreen: menuSportScreen)

        let IntroSportScreen = VisitedScreen(title: "IntroSport", properties: ["univers":"sport"])
        myFeelBack?.setCurrentScreen(currentScreen: IntroSportScreen)

        let menuPhotoScreen = VisitedScreen(title: "menu", properties: ["univers":"photo"])
        myFeelBack?.setCurrentScreen(currentScreen: menuPhotoScreen)

        let introPhoto = VisitedScreen(title: "IntroPhoto", properties: ["univers":"photo"])
        myFeelBack?.setCurrentScreen(currentScreen: introPhoto)

        myFeelBack?.evaluate()
        
        Thread.sleep(forTimeInterval: Double(1))

        let history = myFeelBack?.getSavedHistory()

        XCTAssertEqual(history?.getVisitedScreen().count, 5)
        XCTAssertEqual(history?.getLastSessionId(),1)

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
        
    }
    
    
    func testMinPageVisited3() {
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioMinPageVisited3") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)
        myFeelBack?.clearSavedHistory()
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack?.setNewSession()
        
        let login = VisitedScreen(title: "login", properties: nil)
        myFeelBack?.setCurrentScreen(currentScreen: login)

        let menuSportScreen = VisitedScreen(title: "menu", properties: ["univers":"sport"])
        myFeelBack?.setCurrentScreen(currentScreen: menuSportScreen)

        let IntroSportScreen = VisitedScreen(title: "IntroSport", properties: ["univers":"sport"])
        myFeelBack?.setCurrentScreen(currentScreen: IntroSportScreen)

        let menuPhotoScreen = VisitedScreen(title: "menu", properties: ["univers":"photo"])
        myFeelBack?.setCurrentScreen(currentScreen: menuPhotoScreen)

        let introPhoto = VisitedScreen(title: "IntroPhoto", properties: ["univers":"photo"])
        myFeelBack?.setCurrentScreen(currentScreen: introPhoto)

        myFeelBack?.evaluate()
        
        Thread.sleep(forTimeInterval: Double(1))

        let history = myFeelBack?.getSavedHistory()

        XCTAssertEqual(history?.getVisitedScreen().count, 5)
        XCTAssertEqual(history?.getLastSessionId(),1)

        XCTAssertFalse(fakeCallBack!.isWasSuccessfull())

    }
    
    func testMinPageVisited() {
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioMinPageVisited1_2") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)
        myFeelBack?.clearSavedHistory()
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack?.setNewSession()
        
        let login = VisitedScreen(title: "login", properties: nil)
        myFeelBack?.setCurrentScreen(currentScreen: login)

        let menuSportScreen = VisitedScreen(title: "menu", properties: ["univers":"sport"])
        myFeelBack?.setCurrentScreen(currentScreen: menuSportScreen)

        let IntroSportScreen = VisitedScreen(title: "IntroSport", properties: ["univers":"sport"])
        myFeelBack?.setCurrentScreen(currentScreen: IntroSportScreen)

        let menuPhotoScreen = VisitedScreen(title: "menu", properties: ["univers":"photo"])
        myFeelBack?.setCurrentScreen(currentScreen: menuPhotoScreen)

        let introPhoto = VisitedScreen(title: "IntroPhoto", properties: ["univers":"photo"])
        myFeelBack?.setCurrentScreen(currentScreen: introPhoto)

        myFeelBack?.evaluate()
        
        Thread.sleep(forTimeInterval: Double(1))

        let history = myFeelBack?.getSavedHistory()

        XCTAssertEqual(history?.getVisitedScreen().count, 5)
        XCTAssertEqual(history?.getLastSessionId(),1)

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
    }
    
}
