//
//  PlatformTests.swift
//  MyFeelBackPodTests
//
//  Created by Malek BARKAOUI on 02/04/2021.
//

import XCTest
import Foundation
import OHHTTPStubs

@testable import MyFeelBackPod

class PlatformTests: XCTestCase {
    var myFeelBack: MyFeelBack?
    var fakeCallBack:FakeCallBack?
    var dataManager:LocalDataManager?

    
    override func setUp() {
        fakeCallBack = FakeCallBack()
    }
    
    func testScenarioFalsePlateform() {
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioPlateformeFalse") else {
           return
       }
        

        
        myFeelBack = MyFeelBack(data: scenarioData)

        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack!.evaluate()
        
        Thread.sleep(forTimeInterval: Double(1))

        XCTAssertFalse(fakeCallBack!.isWasSuccessfull())

    }
    
    func testScenarioTruePlateform() {
        
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioPlateforme") else {
           return
       }
        

        
        myFeelBack = MyFeelBack(data: scenarioData)

        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack!.evaluate()
        
        Thread.sleep(forTimeInterval: Double(1))

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
    }
    
}
