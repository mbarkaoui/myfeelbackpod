//
//  VarsDatasTests.swift
//  MyFeelBackPodTests
//
//  Created by Malek BARKAOUI on 23/03/2021.
//

import XCTest

import XCTest
import ObjectiveC
import Foundation
import OHHTTPStubs

@testable import MyFeelBackPod

class VarsDatasTests: XCTestCase {
    var myFeelBack: MyFeelBack?
    var dataManager:LocalDataManager?
    var fakeCallBack:FakeCallBack?
    
    
    override func setUp() {
        fakeCallBack = FakeCallBack()
    }
    
    
    func testVarsDataMultipleBools() {
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioVarsDatasBoolMultiple") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)


        let boolM1 = [ObjCBool(true),ObjCBool(true),ObjCBool(true)]
        let boolM2 = [ObjCBool(true),ObjCBool(true),ObjCBool(true)]
        let boolM3 = [ObjCBool(true),ObjCBool(true),ObjCBool(true)]
        let boolM4 = [ObjCBool(true),ObjCBool(true),ObjCBool(true)]
        let boolM5 = [ObjCBool(true),ObjCBool(false)]
        let boolM6 = [ObjCBool(false),ObjCBool(true)]
        let boolM7 = [ObjCBool(true),ObjCBool(false),ObjCBool(true)]
        let boolM8 = [ObjCBool(true),ObjCBool(false),ObjCBool(true)]
        
        let boolM9 = [ObjCBool(true),ObjCBool(false),ObjCBool(true)]
        let boolM10 = [ObjCBool(false),ObjCBool(true),ObjCBool(false)]



        let dictionary : [String : Any] = [
            "boolM1":boolM1,
            "boolM2":boolM2,
            "boolM3":boolM3,
            "boolM4":boolM4,
            "boolM5":boolM5,
            "boolM6":boolM6,
            "boolM7":boolM7,
            "boolM8":boolM8,
            "boolM9":boolM9,
            "boolM10":boolM10,
        ]

        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        
        myFeelBack?.setContext(context: dictionary)
        myFeelBack!.evaluate()
        
        Thread.sleep(forTimeInterval: Double(1))

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
        XCTAssertTrue(fakeCallBack!.isHandledCalled())
    }
    
    func testVarsDataMultipleStrings() {
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioVarsDatasStringMultiple") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)

        let multiText1 = ["Malek","Hamza","Amany"]
        let multiText2 = ["Malek","Hamza","Amany"]
        let multiText3 = ["ab","ab","ab"]
        let multiText4 = ["AB","AB","AB"]
        let multiText7 = ["malekBarkaoui","qqsdqs","amany"]
        let multiText8 = ["malekBarkaoui","qqsdqs","amany"]
        let multiText9 = ["b","a"]


        let dictionary : [String : Any] = [
            "chaineM1":multiText1,
            "chaineM2":multiText2,
            "chaineM3":multiText3,
            "chaineM4":multiText4,
            "chaineM5":multiText3,
            "chaineM6":multiText4,
            "chaineM7":multiText7,
            "chaineM8":multiText8,
            "chaineM9":multiText9
        ]

        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        
        myFeelBack?.setContext(context: dictionary)
        myFeelBack!.evaluate()
        Thread.sleep(forTimeInterval: Double(1))

        XCTAssertTrue(fakeCallBack!.isHandledCalled())
        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
        
    }
    
    func testVarsDataDate() {
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioVarsDatasDate") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)
  

        let date1 = "2019-03-27T22:31:17"
        let date2 = "2020-03-27T22:31:17"
        let date3 = "2019-03-27T22:31:17"
        let date4 = "2019-03-27T22:31:17"
        let date5 = "2020-03-27T22:31:17"
        let date6 = "2019-03-27T22:31:17"
        let date7 = "2021-03-27T22:31:17"
        let date8 = "2019-03-27T22:31:17"
        let date9 = "2019-03-27T22:31:17"
        let date10 = "2022-03-27T22:31:17"


        let dictionary : [String : Any] = [
            "date10":date10.date!,
            "date9":date9.date!,
            "date8":date8.date!,
            "date7":date7.date!,
            "date6":date6.date!,
            "date5":date5.date!,
            "date4":date4.date!,
            "date3":date3.date!,
            "date2":date2.date!,
            "date1":date1.date!
        ]

        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack?.setContext(context: dictionary)
        myFeelBack!.evaluate()
        Thread.sleep(forTimeInterval: Double(1))

        XCTAssertTrue(fakeCallBack!.isHandledCalled())
        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
    }
    
}
