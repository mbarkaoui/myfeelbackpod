//
//  MyFeelBackPodTests.swift
//  MyFeelBackPodTests
//
//  Created by Malek BARKAOUI on 07/03/2021.
//

import XCTest
import ObjectiveC
import Foundation
import OHHTTPStubs

@testable import MyFeelBackPod

class MyFeelBackPodTests: XCTestCase {
    var myFeelBack: MyFeelBack?
    var dataManager:LocalDataManager?
    var fakeCallBack:FakeCallBack?
    
    
    override func setUp() {
        fakeCallBack = FakeCallBack()
    }
    
    
    func testVarsDataSimpleBools() {
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioVarsDatasBoolSimple") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)

        let boolM1 = ObjCBool(true)
        let boolM2 = ObjCBool(false)


        let dictionary : [String : Any] = [
            "boolM1":boolM1,
            "boolM2":boolM2
        ]

        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        
        myFeelBack?.setContext(context: dictionary)
        myFeelBack!.evaluate()
        
        Thread.sleep(forTimeInterval: Double(1))

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
    }
    func testScenarioFalsePlateform() {
     
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioPlateformeFalse") else {
           return
       }
        
        myFeelBack = MyFeelBack(data: scenarioData)
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack!.evaluate()
        Thread.sleep(forTimeInterval: Double(1))

        XCTAssertFalse(fakeCallBack!.isWasSuccessfull())
    }
    
    func testScenarioTruePlateform() {
      
       dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioPlateforme") else {
           return
       }
        myFeelBack = MyFeelBack(data: scenarioData)
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack!.evaluate()
        Thread.sleep(forTimeInterval: Double(1))

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
    }

     func testScenarioFalse() {
        
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioFaux") else {
           return
       }
        

        myFeelBack = MyFeelBack(data: scenarioData)
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack!.evaluate()
        Thread.sleep(forTimeInterval: Double(1))

        XCTAssertFalse(fakeCallBack!.isWasSuccessfull())
     }
    
    func testScenarioFalse1() {
      
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioFaux1") else {
           return
       }
        

        myFeelBack = MyFeelBack(data: scenarioData)
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack!.evaluate()
        Thread.sleep(forTimeInterval: Double(1))

        XCTAssertFalse(fakeCallBack!.isWasSuccessfull())
    }
    
    func testScenarioVrai2() {
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "Scenario-vrai2") else {
           return
       }
        

        myFeelBack = MyFeelBack(data: scenarioData)

        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack!.evaluate()
        Thread.sleep(forTimeInterval: Double(1))

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
    }
    
    func testScenarioVrai3() {
        
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "Scenario-vrai3") else {
           return
       }
        
        myFeelBack = MyFeelBack(data: scenarioData)
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack!.evaluate()

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
    }

 }


