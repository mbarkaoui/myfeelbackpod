//
//  FakeCallback.swift
//  MyFeelBackPodTests
//
//  Created by Malek BARKAOUI on 19/03/2021.
//

import Foundation
import MyFeelBackPod

public class FakeCallBack:MyFeelBackProtocol {
    private var wasSuccessfull = false
    private var handleCalled = false
    public var completionCount = 0

    public func evalCompletion(resultEval: Bool) {
        print("finish evaluation \(resultEval)" )
        handleCalled = true
        wasSuccessfull = resultEval
        completionCount += 1
    }
    
    public func isPassedOneTimeInCompletion() -> Bool {
        return completionCount == 1
    }
    
    public func nothingHappened() -> Bool {
        return completionCount == 0
    }
    
    public func isWasSuccessfull() -> Bool {
        return wasSuccessfull
    }
    
    public func isHandledCalled() -> Bool {
        return handleCalled
    }
    
    
}
