//
//  TimeoutMinTests.swift
//  MyFeelBackPodTests
//
//  Created by Malek BARKAOUI on 23/03/2021.
//

import XCTest
import Foundation
import OHHTTPStubs

@testable import MyFeelBackPod

class TimeoutMinTests: XCTestCase {
    var myFeelBack: MyFeelBack?
    var dataManager:LocalDataManager?
    var fakeCallBack:FakeCallBack?
    
    
    override func setUp() {
        fakeCallBack = FakeCallBack()
    }
    
    func testTimeoutmin1() {
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioTimeoutmin1") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)

        
        let attr1 = "A"
 
        let dictionary : [String : Any] = ["attr1":attr1]
        fakeCallBack = FakeCallBack()

      //  myFeelBack = MyFeelBack(urlString: scenarioData)
        myFeelBack?.setContext(context: dictionary)
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack!.evaluate()
        
        Thread.sleep(forTimeInterval: Double(11))
        
        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
        XCTAssertTrue(fakeCallBack!.isHandledCalled())
    }
    
    
    func testTimeoutmin2() {
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioTimeoutmin1") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)

        
        let attr1 = "B"
 
        let dictionary : [String : Any] = ["attr1":attr1]
        fakeCallBack = FakeCallBack()

        //myFeelBack = MyFeelBack(urlString: scenarioData)
        myFeelBack?.setContext(context: dictionary)
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack!.evaluate()

        Thread.sleep(forTimeInterval: Double(21))

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
        XCTAssertTrue(fakeCallBack!.isHandledCalled())
        
    }
    
    func testTimeoutmin4() {
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioTimeoutmin4") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)

        
        let attr1 = "A"
 
        let dictionary : [String : Any] = ["attr1":attr1]
        fakeCallBack = FakeCallBack()

        myFeelBack?.setContext(context: dictionary)
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack!.evaluate()

        Thread.sleep(forTimeInterval: Double(11))

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
        XCTAssertTrue(fakeCallBack!.isHandledCalled())
        
    }
    
    
    func testTimeoutmin5() {
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioTimeoutmin4") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)
        
 
        let attr1 = "B"
 
        let dictionary : [String : Any] = ["attr1":attr1]
        fakeCallBack = FakeCallBack()

        myFeelBack?.setContext(context: dictionary)
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack!.evaluate()

        Thread.sleep(forTimeInterval: Double(21))

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
        XCTAssertTrue(fakeCallBack!.isHandledCalled())
        
    }
    
    func testTimeoutmin7() {
        
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioTimeoutmin7_8") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)

   
        let attr1 = "A"
 
        let dictionary : [String : Any] = ["attr1":attr1]
        fakeCallBack = FakeCallBack()
        myFeelBack?.setContext(context: dictionary)
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        Thread.sleep(forTimeInterval: Double(3))

        myFeelBack!.evaluate()
        
        Thread.sleep(forTimeInterval: Double(11))

        XCTAssertTrue(fakeCallBack!.isWasSuccessfull())
        XCTAssertTrue(fakeCallBack!.isHandledCalled())
        
    }
    
    
    
    func testTimeoutmin9() {
        
        dataManager = LocalDataManager()
       guard  let scenarioData = dataManager?.readLocalFile(forName: "ScenarioTimeoutmin9") else {
           return
       }

        myFeelBack = MyFeelBack(data: scenarioData)

        
        let attr1 = "C"
        let dictionary : [String : Any] = ["attr1":attr1]
        fakeCallBack = FakeCallBack()
        myFeelBack?.setContext(context: dictionary)
        myFeelBack?.setDelegate(delegate: fakeCallBack!)
        myFeelBack!.evaluate()

        Thread.sleep(forTimeInterval: Double(21))

        XCTAssertFalse(fakeCallBack!.isWasSuccessfull())
        XCTAssertFalse(fakeCallBack!.isHandledCalled())
        
        
    }
}
